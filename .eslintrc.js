module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
  },
  extends: ['standard', 'plugin:vue/essential', 'plugin:prettier/recommended'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
  },
  plugins: ['vue'],
  rules: {
    // 禁止使用console, 1
    'no-console': 0,
    // 句尾是否需要分号，1
    semi: 0,
    // 函数参数空格问题
    // 匿名函数0，具名函数0，箭头函数1
    'space-before-function-paren': ['error', { anonymous: 'never', named: 'never', asyncArrow: 'always' }],
    // 对象属性最后一位是否可以有尾随逗号，1
    'comma-dangle': 0,
    camelcase: 0,
    // 允许async await
    'generator-star-spacing': 'off',
    'no-shadow': 0,
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'arrow-parens': 0,
    eqeqeq: 0,
    'no-multiple-empty-lines': [0, { max: 1 }],
  },
};
